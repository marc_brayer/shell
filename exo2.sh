#!/bin/bash

#FONCTION 1 --- Déclaration de la fonction qui détecte le nom utilisateur#
#test marc

function fonction1
{
echo "Veuillez entrer le nom d'utilisateur : "
read username

number=$(grep -c $username /etc/passwd)

if [ $number != 0 ]
then
echo "L'utilisateur existe."
else
echo "L'utilisateur n'existe pas."
fi
}



#FONCTION 2 --- Sortir le UID utilisateur#

function fonction2
{
echo "Veuillez entrer le nom d'utilisateur : "
read username

number=$(grep ^$username /etc/passwd | cut -d":" -f3)
echo "Le GUI de $username est $number"
}


#MENU#

a=0
while [ $a != 1 ] && [ $a != 2 ] && [ $a != q ]
do
echo -e  "\n1 - Vérifier l'existence d'un utilisateur\n2 - Connaitre son UID\nq - Quitter\n"
read a
done

if [ $a == 1 ]; then
fonction1
elif [ $a == 2 ]; then
fonction2
else
exit
fi
